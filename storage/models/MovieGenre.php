<?php namespace App;

/**
 * Eloquent class to describe the movie_genre table
 *
 * automatically generated by ModelGenerator.php
 */
class MovieGenre extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'movie_genre';

    public $primaryKey = '';

    public $timestamps = false;

    public $incrementing = false;

    protected $fillable = array();

    public function movie()
    {
        return $this->belongsTo('App\Movie', 'movie_id', 'id');
    }

    public function genre()
    {
        return $this->belongsTo('App\Genre', 'genre_id', 'id');
    }
}
